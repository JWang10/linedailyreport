## Line Daily Report

## Perequisite

- 需要建立一個GCP專案取得服務憑證（.json）,及啓用Google Sheet API服務（ref：）

    <img src="assets/image-20200415145853261.png" alt="image-20200415145853261" style="zoom: 50%;" />

    - `.json`

        <img src="assets/image-20200415152337282.png" alt="image-20200415152337282" style="zoom:50%;" />

- Google 表單共用分享

    - 表單共用分享給服務憑證裏面的`client_email` 

    <img src="assets/image-20200415152406443.png" alt="image-20200415152406443" style="zoom:50%;" />
    
- 儲存表單ID（下圖紅框處爲表單ID）

    ![image-20200415154015647](assets/image-20200415154015647.png)

- 使用`pip install`安裝相關套件

    或是`pip install -r requirements.txt`從專案文件裏面安裝

    ```python
    Flask==1.1.1
    line-bot-sdk==1.16.0
    gspread==3.4.2
    oauth2client==4.1.3
    gunicorn==20.0.4
    pytz==2019.3
    ```

## Steps

- 需調整`LineDailyReport.py`

    如果要放在`heroku`，需設定`Config Vars`
    <img src="assets/image-20200415150929339.png" alt="image-20200415150929339" style="zoom:50%;" />

    ```python
    # input channel_access_token
    line_bot_api = LineBotApi(os.environ['token'])
    # input channel_secret
    handler = WebhookHandler(os.environ['secret'])
    # input a port you like
    app.run(host='0.0.0.0', port=os.environ['PORT'])
    ```

- 調整`LineDailyReport.py`表單ID的路徑

    ```sh
    spreadsheet_key_path= <google sheet id>
    ```

- （可選）如果要跑在`heroku`，至少要在根目錄配置兩個文件**

    - **requirements.txt** 


    - **Procfile**

     requirements.txt 

    ​	這個檔案是要告訴 [Heroku](https://dashboard.heroku.com/) 你的環境需要那些其他的套件

     Procfile

    ​	這個檔案是要告訴 [Heroku](https://dashboard.heroku.com/) 要如何啟動這個 web app

     	Procfile 檔案，基本使用方法如下

    ```sh
    web: gunicorn <app name>:app
    ```

- 使用`Heroku CLI `部署檔案到`Heroku`

- 部署完成，網址格式爲如下圖

    ```
    https://< heroku app >.herokuapp.com/
    ```

    <img src="assets/image-20200415151701588.png" alt="image-20200415151701588" style="zoom:50%;" />

- 綁定`Line`與`Heroku`

    <img src="assets/image-20200415151946081.png" alt="image-20200415151946081" style="zoom:50%;" />

## Demo

### 三種指令模式

- 每日工作狀況回應表
- 工作進度週報
- help

<img src="assets/image-20200415114433949.png" alt="image-20200415114433949" style="zoom:50%;" />

- **輸入正確，會回饋新增筆數的訊息**

    <img src="assets/image-20200415114532831.png" alt="image-20200415114532831" style="zoom:50%;" />

[影片參考](http://georobot.geosat.com.tw:8882/LineDailyReport.mp4)

## Note

- 因爲目前是用**line輕量免費版，<del>一個月只有500則訊息量</del>(主動推播的次數計算)**，超過使用則不會得到訊息回饋，這種情況需使用這自己在手動上單

    <img src="assets/image-20200415000518480.png" alt="image-20200415000518480" style="zoom: 50%;" />

- 另外使用**line api接後端程式一樣需要使用`https`協定的服務**，目前測試皆採用proxy方式，發佈放在[`heroku`](https://www.heroku.com/)


- 免費`heroku`限制 **閒置30分鐘後進入睡眠狀態，否則取決於剩餘的每月免費dyno小時**，所以在**閒置的時候傳遞訊息，需等待`heroku server`啓動才會回應**