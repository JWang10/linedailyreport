import os
from flask import Flask, request, abort

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
)

import sys
import gspread
from oauth2client.service_account import ServiceAccountCredentials as SAC
from datetime import datetime
from pytz import timezone

app = Flask(__name__)

line_bot_api = LineBotApi(os.environ['token'])
handler = WebhookHandler(os.environ['secret'])


@app.route("/", methods=['GET'])
def hello():
    return "Hello World!"


@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        print(InvalidSignatureError)
        abort(400)

    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    # line_bot_api.reply_message(
    #     event.reply_token,
    #     TextSendMessage(text=event.message.text))

    # load sheet data
    sheet = ['每日工作狀況回應表', '週報工作進度報告_系統整合部']
    # trim '\n'
    texts = event.message.text.replace('\n', '').split(',')
    GDriveJSON = 'linedailyreportBot.json'
    # GSpreadSheet = 'LineTest'

    # time
    tzone = datetime.now(timezone('Asia/Taipei'))
    _today = tzone.strftime("%Y/%m/%d %H:%M:%S")

    if texts[0] == "每日工作狀況回應表":
        '''
        # Input format
        # 輸入格式為：
        # 1.   email
        # 2.    日期
        # 3.    部門
        # 4.    同仁姓名
        # 5.    是否有請假
        # 6.    假別
        # 7.    假別時間
        # 8.    起迄地點
        # 9.    事由
        # 10.   交通工具
        '''
        _email = ""
        _date = ""
        # _department = ""
        _name = ""
        _isattend = ""
        _type = ""
        _typetime = ""
        _location = ""
        _reason = ""
        _vehicle = ""

        try:
            if texts[1] is not "":
                _email = texts[1]
            if texts[2] is not "":
                _date = texts[2]
            if texts[3] is not "":
                _name = texts[3]
            if texts[4] is not "":
                _isattend = texts[4]
            if texts[5] is not "":
                _type = texts[5]
            if texts[6] is not "":
                _typetime = texts[6]
            if texts[7] is not "":
                _location = texts[7]
            if texts[8] is not "":
                _reason = texts[8]
            if texts[9] is not "":
                _vehicle = texts[9]
        except Exception as ex:
            print(ex)

        spreadsheet_key_path = './spreadsheetkey_daily.txt'

        # while True:
        try:
            gss_scopes = ['https://spreadsheets.google.com/feeds']
            gss_client = auth_gss_client(GDriveJSON, gss_scopes)
        except Exception as ex:
            print('無法連線Google試算表', ex)
            sys.exit(1)

        with open(spreadsheet_key_path) as f:
            spreadsheet_key = f.read().strip()

        update_sheet_daily(gss_client, key=spreadsheet_key, email=_email, today=_today, date=_date,
                           name=_name, department="系統整合部", isNotAttendonTime=_isattend,
                           type=_type, typetime=_typetime, location=_location, reason=_reason, vehicle=_vehicle)

        print('新增一筆資料到試算表', sheet[0])
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text='新增一筆資料到試算表 '+sheet[0]))

    elif texts[0] == "工作進度週報":
        '''
        # Input format
        # 輸入格式為：
        # email
        # 姓名
        # 專案名稱
        # 本週完成工作進度
        # 下週預計完成進度
        '''

        _email = ""
        _name = ""
        _project = ""
        _currProgree = ""
        _nextProgress = ""
        try:
            if texts[1] is not "":
                _email = texts[1]
            if texts[2] is not "":
                _name = texts[2]
            if texts[3] is not "":
                _project = texts[3]
            if texts[4] is not "":
                _currProgree = texts[4]
            if texts[5] is not "":
                _nextProgress = texts[5]
        except Exception as ex:
            print(ex)

        spreadsheet_key_path = './spreadsheetkey_week.txt'
        # while True:
        try:
            gss_scopes = ['https://spreadsheets.google.com/feeds']
            gss_client = auth_gss_client(GDriveJSON, gss_scopes)
        except Exception as ex:
            print('無法連線Google試算表', ex)
            sys.exit(1)

        with open(spreadsheet_key_path) as f:
            spreadsheet_key = f.read().strip()

        update_sheet_week(gss_client, key=spreadsheet_key, today=_today, email=_email,
                          name=_name, project=_project, currProgress=_currProgree,
                          nextProgress=_nextProgress)

        print('新增一筆資料到試算表', sheet[1])
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text='新增一筆資料到試算表 '+sheet[1]))
    elif texts[0] == "help":

        txt = u"兩種輸入格式爲:\n"
        txt += "1)每日工作狀況回應表\n"
        txt += u"email,\n日期,\n部門,\n同仁姓名,\n是否有請假,\n" +\
            u"假別,\n假別時間,\n起迄地點,\n事由,\n交通工具\n"
        txt += "\n\n2)工作進度週報\n"
        txt += u"email,\n姓名,\n專案名稱,\n本週完成工作進度,\n" +\
            u"下週預計完成進度\n"

        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=txt))
        txt = ""


def auth_gss_client(path, scopes):
    credentials = SAC.from_json_keyfile_name(path, scopes)
    return gspread.authorize(credentials)

# type: 假別
# typetime: 假別時間
def update_sheet_daily(gss_client, key, today, email, date, name, department="系統整合部", isNotAttendonTime="否",
                       type="正常到班", typetime="", location="", reason="", vehicle=""):
    wks = gss_client.open_by_key(key)
    sheet = wks.sheet1
    sheet.append_row([today, email,
                      date, department, name, isNotAttendonTime, type, typetime, location, reason, vehicle])


def update_sheet_week(gss_client, key, today, email, name, project, currProgress, nextProgress):
    wks = gss_client.open_by_key(key)
    sheet = wks.sheet1
    sheet.append_row([today, email,
                      name, project, currProgress, nextProgress])


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=os.environ['PORT'])
